from fastapi import APIRouter
from .get_orders import add_orders

orders_router = APIRouter(
    tags=['Заявки на проводку']
)

orders_router.add_api_route(
    path='/orders',
    endpoint=add_orders,
    methods=['POST'],
    tags=['Заявки на проводку']
)
