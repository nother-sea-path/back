from fastapi import APIRouter
from back.api.orders import orders_router

api_router = APIRouter()


api_router.include_router(orders_router, prefix='/orders')

