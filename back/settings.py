from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT :int = 4300
    ENABLE_AUTORELOAD :bool = True
    WORKERS :int = 1

    POSTGRES_URL :str = 'postgresql+asyncpg://ntr:Serial111@127.0.0.1:55432/ntr'
    model_config = SettingsConfigDict(env_prefix='NTR_')

settings = Settings()